import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from 'src/user/schema/user.schema';
import { ROLE_PROJECT, STATUS } from 'src/utils/enum';

export type ProjectDocument = Project & Document;
export type UserProject = {
  userId: mongoose.Schema.Types.ObjectId;

  role: number;
};
@Schema()
export class Project {
  @Prop({ required: true })
  name: string;
  @Prop(
    raw({
      user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      role: { type: Object.values(ROLE_PROJECT) },
    }),
  )
  users: Record<string, any>;

  @Prop({ type: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel' } })
  channels;

  @Prop({ enum: Object.values(STATUS) })
  status: number;
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
