export const ROLE = {
  ADMIN: 1,
  USER: 2,
};
export const STATUS = {
  ACTIVE: 1,
  DEACTIVE: 0,
  DELETE: -1,
};
export const GENDER = {
  OTHER: 0,
  MALE: 1,
  FEMALE: 2,
};
export const ROLE_PROJECT = {
  ADMIN: 1,
  MEMBER: 2,
};
