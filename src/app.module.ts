import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { ProjectModule } from './project/project.module';
import { ChannelModule } from './channel/channel.module';
import { CustomerModule } from './customer/customer.module';
import { ConversationModule } from './conversation/conversation.module';
import { MessageModule } from './message/message.module';
import { VaConfigModule } from './va-config/va-config.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb://128.199.138.254:30001/va-nestjs?replicaSet=rs0',
      {
      },
    ),
    UserModule,
    ProjectModule,
    ChannelModule,
    CustomerModule,
    ConversationModule,
    MessageModule,
    VaConfigModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
