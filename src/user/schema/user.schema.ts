import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { GENDER, ROLE, STATUS } from 'src/utils/enum';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true, unique: true, index: true })
  email: string;

  @Prop()
  phoneNumber: string;

  @Prop()
  avatar: string;

  @Prop()
  username: string;

  @Prop({ enum: Object.values(GENDER) })
  gender: number;

  @Prop({ required: true })
  password: string;

  @Prop({ enum: Object.values(ROLE), default: ROLE.USER })
  role: number;

  @Prop({ enum: Object.values(STATUS), default: STATUS.DEACTIVE })
  status: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
