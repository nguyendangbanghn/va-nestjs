import {
  forwardRef,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { InjectModel, MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema/user.schema';
import { Connection, Model, model } from 'mongoose';
import { UniqueEmail } from './middleware/unique-email.middleware';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: User.name,

        useFactory: async () => {
          const schema = UserSchema;
          // const Model = model('user');
          schema.pre('save', async function (next) {
            const a = await Model.find();

            // console.log(a);
            next();
          });
          return schema;
        },
      },
    ]),
  ],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(UniqueEmail).forRoutes({
      path: 'user',
      method: RequestMethod.POST,
    });
  }
}
