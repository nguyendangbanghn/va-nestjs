import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schema/user.schema';
import { UserModule } from './user.module';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserModule>) {}

  create(createUserDto: CreateUserDto) {
    try {
      const createUser = new this.userModel(createUserDto);
      return createUser.save();
    } catch (error) {
      console.log(123)
    }
  }

  findAll() {
    return `This action returns all user`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
