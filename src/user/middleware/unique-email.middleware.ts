import { Injectable, NestMiddleware } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Request, Response, NextFunction } from 'express';
import { Model } from 'mongoose';
import { User } from '../schema/user.schema';
import { UserModule } from '../user.module';

@Injectable()
export class UniqueEmail implements NestMiddleware {
  constructor(@InjectModel(User.name) private userModel: Model<UserModule>) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const newUser: User = req.body;

    console.log(await this.userModel.exists({ email: newUser.email }));

    next();
  }
}
