import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import * as validator from 'class-validator';
import { GENDER, ROLE, STATUS } from 'src/utils/enum';

export class CreateUserDto {
  @ApiProperty({ required: true, minLength: 5, description: 'email format' })
  @validator.IsString()
  @validator.IsEmail()
  @validator.IsNotEmpty()
  email: string;

  @ApiProperty({ minLength: 6, description: 'Not null' })
  @validator.IsString()
  @validator.IsNotEmpty()
  @validator.MinLength(6)
  password: string;

  @ApiProperty({ description: 'phoneNumber must be a phone number' })
  @validator.IsOptional()
  @validator.IsString()
  @validator.IsMobilePhone('vi-VN')
  phoneNumber: string;

  @ApiProperty()
  @validator.IsOptional()
  @validator.IsString()
  avatar: string;

  @ApiProperty({
    description: JSON.stringify(GENDER),
  })
  @validator.IsEnum(Object.values(GENDER))
  gender: number = GENDER.OTHER;

  @ApiProperty({
    description: JSON.stringify(ROLE),
    default: ROLE.USER,
    readOnly: true,
  })
  @validator.Equals(ROLE.USER)
  role: number = ROLE.USER;

  @ApiProperty({
    readOnly: true,
    description: JSON.stringify(STATUS),
    default: STATUS.DEACTIVE,
  })
  @validator.Equals(STATUS.DEACTIVE)
  status: number=STATUS.DEACTIVE;
}
