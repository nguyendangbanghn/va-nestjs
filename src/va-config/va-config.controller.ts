import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { VaConfigService } from './va-config.service';
import { CreateVaConfigDto } from './dto/create-va-config.dto';
import { UpdateVaConfigDto } from './dto/update-va-config.dto';

@Controller('va-config')
export class VaConfigController {
  constructor(private readonly vaConfigService: VaConfigService) {}

  @Post()
  create(@Body() createVaConfigDto: CreateVaConfigDto) {
    return this.vaConfigService.create(createVaConfigDto);
  }

  @Get()
  findAll() {
    return this.vaConfigService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.vaConfigService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVaConfigDto: UpdateVaConfigDto) {
    return this.vaConfigService.update(+id, updateVaConfigDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vaConfigService.remove(+id);
  }
}
