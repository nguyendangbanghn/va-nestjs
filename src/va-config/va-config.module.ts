import { Module } from '@nestjs/common';
import { VaConfigService } from './va-config.service';
import { VaConfigController } from './va-config.controller';

@Module({
  controllers: [VaConfigController],
  providers: [VaConfigService]
})
export class VaConfigModule {}
