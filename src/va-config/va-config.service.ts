import { Injectable } from '@nestjs/common';
import { CreateVaConfigDto } from './dto/create-va-config.dto';
import { UpdateVaConfigDto } from './dto/update-va-config.dto';

@Injectable()
export class VaConfigService {
  create(createVaConfigDto: CreateVaConfigDto) {
    return 'This action adds a new vaConfig';
  }

  findAll() {
    return `This action returns all vaConfig`;
  }

  findOne(id: number) {
    return `This action returns a #${id} vaConfig`;
  }

  update(id: number, updateVaConfigDto: UpdateVaConfigDto) {
    return `This action updates a #${id} vaConfig`;
  }

  remove(id: number) {
    return `This action removes a #${id} vaConfig`;
  }
}
