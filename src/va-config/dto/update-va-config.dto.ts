import { PartialType } from '@nestjs/mapped-types';
import { CreateVaConfigDto } from './create-va-config.dto';

export class UpdateVaConfigDto extends PartialType(CreateVaConfigDto) {}
